Ryan Emch
Undergraduate of Computer Science

Homework 08


1. This assignment took me around 5 days to complete.

2. The hardest part was determining whether each logical expression valid or not.

3. Propositional Logic converts a complete sentence into a symbol and makes it logical whereas in First-Order Logic relation of a particular sentence will be made that involves relations, constants, functions, and constants.

4. Quantifier symbols: ∀ for universal quantification, and ∃ for existential quantification.
Logical connectives: ∧ for conjunction, ∨ for disjunction, → for implication, ↔ for bi-conditional, ¬ for negation. Parentheses, brackets, and other punctuation symbols.
